from http import HTTPStatus


def test_health_check(client):
    response = client.get("/health_check")
    assert HTTPStatus.OK == response.status_code
    assert {"status": "ok"} == response.json


def test_encrypt_one_phrase(client):
    response = client.get("/encrypt?text=teste teste")
    assert HTTPStatus.OK == response.status_code
    assert {"result": "- . ... - . / - . ... - ."} == response.json


def test_encrypt_without_arg_required(client):
    response = client.get("/encrypt")
    assert HTTPStatus.BAD_REQUEST == response.status_code
    result = {
        "validation_error": {
            "query_params": [
                {"loc": ["text"], "msg": "field required", "type": "value_error.missing"}
            ]
        }
    }
    assert result == response.json


def test_encrypt_error(client):
    response = client.get("/encrypt?text=ç")
    assert HTTPStatus.BAD_REQUEST == response.status_code
    assert response.json == {"error": "Unable to encrypt text"}


def test_decrypt_one_phrase(client):
    response = client.get("/decrypt?text=- . ... - . / - . ... - .")
    assert HTTPStatus.OK == response.status_code
    assert {"result": "TESTE TESTE"} == response.json


def test_decrypt_without_arg_required(client):
    response = client.get("/decrypt")
    assert HTTPStatus.BAD_REQUEST == response.status_code
    result = {
        "validation_error": {
            "query_params": [
                {"loc": ["text"], "msg": "field required", "type": "value_error.missing"}
            ]
        }
    }
    assert result == response.json


def test_decrypt_error(client):
    response = client.get("/decrypt?text=......")
    assert HTTPStatus.BAD_REQUEST == response.status_code
    assert response.json == {"error": "Unable to decrypt text"}
