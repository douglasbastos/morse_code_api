import pytest

from src.data.use_cases.morse_code import (
    encrypt,
    decrypt,
)
from src.data.use_cases.erros import EncryptMorseCodeError, DecryptMorseCodeError


def test_encrypt_with_only_one_letter():
    assert encrypt("B") == "-..."


def test_encrypt__lowercase_letters():
    assert encrypt("b") == "-..."


def test_encrypt_with_a_word():
    assert encrypt("TESTE") == "- . ... - ."


def test_encrypt_with_a_phrase():
    assert encrypt("TESTE TESTE") == "- . ... - . / - . ... - ."


def test_encrypt_with_unmapped_character():
    with pytest.raises(EncryptMorseCodeError):
        encrypt("ç")


def test_decrypt_with_only_one_letter():
    assert decrypt("-...") == "B"


def test_decrypt_with_a_word():
    assert decrypt("- . ... - .") == "TESTE"


def test_decrypt_with_a_phrase():
    assert decrypt("- . ... - . / - . ... - .") == "TESTE TESTE"


def test_decrypt_with_unmapped_character():
    with pytest.raises(DecryptMorseCodeError):
        decrypt("--------")
