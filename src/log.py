import logging


def get_logger():
    format = '{"%(levelname)s":"%(asctime)s", "%(funcName)s":%(lineno)d, "%(threadName)s":"%(message)s"}'
    logging.basicConfig(format=format)
    logger = logging.getLogger('ACT-API')
    logger.setLevel(level=logging.INFO)
    return logger


logger = get_logger()
