from http import HTTPStatus

from flasgger import swag_from
from flask import jsonify
from flask_pydantic import validate

from src.api import api_bp
from src.api.schemas import MorseCodeSchema
from src.data.use_cases import morse_code
from src.data.use_cases.erros import EncryptMorseCodeError, DecryptMorseCodeError
from src.log import logger


@api_bp.errorhandler(EncryptMorseCodeError)
@api_bp.errorhandler(DecryptMorseCodeError)
def handle_error(err):
    return jsonify({"error": "|".join(err.args)}), HTTPStatus.BAD_REQUEST


@api_bp.route("/health_check")
@swag_from("specs/health_check.yml")
def health_check():
    logger.info("Endpoint: health_check")
    return jsonify({"status": "ok"})


@api_bp.route("/encrypt")
@validate()
@swag_from("specs/encrypt.yml")
def encrypt(query: MorseCodeSchema):
    logger.info(f"Endpoint: encrypt {query}")
    return jsonify({"result": morse_code.encrypt(text=query.text)})


@api_bp.route("/decrypt")
@validate()
@swag_from("specs/decrypt.yml")
def decrypt(query: MorseCodeSchema):
    logger.info(f"Endpoint: decrypt {query}")
    return jsonify({"result": morse_code.decrypt(text=query.text)})
