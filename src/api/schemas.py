from pydantic import BaseModel


class MorseCodeSchema(BaseModel):
    text: str
