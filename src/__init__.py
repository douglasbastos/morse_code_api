import logging

from flasgger import Swagger
from flask import Flask


def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)
    Swagger(app)

    from src.api import api_bp  # NOQA

    app.register_blueprint(api_bp, url_prefix="/")

    app.logger.setLevel(logging.INFO)
    app.logger.info("Act api startup")

    return app
