from src.data.use_cases.erros import EncryptMorseCodeError, DecryptMorseCodeError
from src.log import logger

MORSE_CODE = {
    "A": ".-",
    "B": "-...",
    "C": "-.-.",
    "D": "-..",
    "E": ".",
    "F": "..-.",
    "G": "--.",
    "H": "....",
    "I": "..",
    "J": ".---",
    "K": "-.-",
    "L": ".-..",
    "M": "--",
    "N": "-.",
    "O": "---",
    "P": ".--.",
    "Q": "--.-",
    "R": ".-.",
    "S": "...",
    "T": "-",
    "U": "..-",
    "V": "...-",
    "W": ".--",
    "X": "-..-",
    "Y": "-.--",
    "Z": "--..",
    "0": "-----",
    "1": ".----",
    "2": "..---",
    "3": "...--",
    "4": "....-",
    "5": ".....",
    "6": "-....",
    "7": "--...",
    "8": "---..",
    "9": "----.",
    " ": "/",
}


def encrypt(text: str) -> str:
    logger.info(f"UseCase: encrypt {text}")
    try:
        return " ".join(MORSE_CODE[i.upper()] for i in text)
    except KeyError:
        raise EncryptMorseCodeError("Unable to encrypt text")


def decrypt(text: str) -> str:
    logger.info(f"UseCase: decrypt {text}")
    morse_code_reversed = dict(map(reversed, MORSE_CODE.items()))
    try:
        return "".join(morse_code_reversed[i] for i in text.split())
    except KeyError:
        raise DecryptMorseCodeError("Unable to decrypt text")
