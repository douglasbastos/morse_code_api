Aplicação simples onde disponibiliza dois endpoints para criptografar e descriptografar Código Morse.

Os endpoints estão documentados e é possível visualizar nesse [Link](http://3.139.75.221/apidocs/#/)

### Requisito

    python >= 3.6

### Instalação em desenvolvimento

    poetry install

### Rodando a aplicação local

    FLASK_APP=app.py flask run

### Caso deseja utilizar docker.

    docker-compose up -d

### Executando os testes unitários

    pytest

Obs: Caso esteja executando diretamente do virtualenv local a aplicação será servida na porta 5000, caso esteja utilizando docker o serviço será exposto na 80.