FROM python:3.9-bullseye

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    libatlas-base-dev gfortran gcc make g++ supervisor uwsgi procps \
    gnupg1 apt-transport-https ca-certificates

RUN pip install gunicorn poetry

ENV HOME=/opt/repo
WORKDIR ${HOME}
EXPOSE 5000

COPY poetry.lock pyproject.toml ./
RUN poetry export -f requirements.txt --output requirements.txt
RUN pip3 install -r ${HOME}/requirements.txt


COPY . ${HOME}/

RUN chmod +x boot.sh
ENTRYPOINT ["./boot.sh"]
